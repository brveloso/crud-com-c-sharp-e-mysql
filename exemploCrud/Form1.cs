﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace exemploCrud
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //inserir dados no combobox do estado
            combEstado.Items.Add("AC");
            combEstado.Items.Add("AL");
            combEstado.Items.Add("AP");
            combEstado.Items.Add("AM");
            combEstado.Items.Add("BA");
            combEstado.Items.Add("CE");
            combEstado.Items.Add("DF");
            combEstado.Items.Add("ES");
            combEstado.Items.Add("GO");
            combEstado.Items.Add("MA");
            combEstado.Items.Add("MT");
            combEstado.Items.Add("MS");
            combEstado.Items.Add("MG");
            combEstado.Items.Add("PA");
            combEstado.Items.Add("PB");
            combEstado.Items.Add("PR");
            combEstado.Items.Add("PE");
            combEstado.Items.Add("PI");
            combEstado.Items.Add("RJ");
            combEstado.Items.Add("RS");
            combEstado.Items.Add("RO");
            combEstado.Items.Add("RR");
            combEstado.Items.Add("SC");
            combEstado.Items.Add("SP");
            combEstado.Items.Add("SE");
            combEstado.Items.Add("TO");

            //INSERIR DADOS NO COMBOBOX DA CIDADE
            combCidade.Items.Add("Cametá");
            combCidade.Items.Add("Belém");
            combCidade.Items.Add("Ananindeua");


        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                //passsa a string de conexão
                MySqlConnection mySqlConnection = new MySqlConnection("server= localhost;" +
                    "port=3306;" +
                    "User Id= root;" +
                    "database=bd_crud;" +
                    "password='';") ;
                //abre o banco de dados
                mySqlConnection.Open();

                //comando sql para inserir dados na tabela
                MySqlCommand objCmd = new MySqlCommand("insert into tb_dados(cd_dados, nm_nome, sg_estado, nm_cidade, ds_endereco) values (null,?,?,?,?)",mySqlConnection);

                //parametros do comando sql
                objCmd.Parameters.Add("@nm_nome",MySqlDbType.VarChar, 60).Value = txtNome.Text;
                objCmd.Parameters.Add("@sg_estado",MySqlDbType.VarChar, 2).Value = combEstado.SelectedItem.ToString();
                objCmd.Parameters.Add("@nm_cidade",MySqlDbType.VarChar, 20).Value = combCidade.SelectedItem.ToString();
                objCmd.Parameters.Add("@ds_endereco",MySqlDbType.VarChar, 60).Value = txtEndereco.Text;

                //comando para executar query
                objCmd.ExecuteNonQuery();

                MessageBox.Show("Conectado");
                //fecha o banco de dados
                mySqlConnection.Close();



            }
            catch(Exception erro)
            {
                MessageBox.Show("Erro: "+erro);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void combEstado_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void delete_Click(object sender, EventArgs e)
        {
            try 
            {
                //passsa a string de conexão
                MySqlConnection mySqlConnection = new MySqlConnection("server= localhost;" +
                    "port=3306;" +
                    "User Id= root;" +
                    "database=bd_crud;" +
                    "password='';");
                //abre o banco de dados
                mySqlConnection.Open();

                //COMANDOS DO MYSQL COM SEUS DEVIDOS PARANMETROS
                MySqlCommand mySqlCommand = new MySqlCommand("delete from tb_dados where cd_dados = ?",mySqlConnection);
                mySqlCommand.Parameters.Clear();
                mySqlCommand.Parameters.Add("@cd_dados",MySqlDbType.Int32).Value = txtCod.Text;
                
                //EXECUTA O COMANDO
                mySqlCommand.CommandType = CommandType.Text;
                mySqlCommand.ExecuteNonQuery();
                //FECHA A CONEXÃO
                mySqlConnection.Close();
                MessageBox.Show("Registro removido com sucesso");

            }
            catch(Exception erro)
            {
                MessageBox.Show("Não foi possivel deletar: "+erro);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                //passsa a string de conexão
                MySqlConnection mySqlConnection = new MySqlConnection("server= localhost;" +
                    "port=3306;" +
                    "User Id= root;" +
                    "database=bd_crud;" +
                    "password='';");
                //abre o banco de dados
                mySqlConnection.Open();
                MySqlCommand mySqlCommand = new MySqlCommand("select nm_nome, sg_estado, nm_cidade, ds_endereco from tb_dados where cd_dados = ?", mySqlConnection);
                mySqlCommand.Parameters.Clear();
                mySqlCommand.Parameters.Add("@cd_dados", MySqlDbType.Int32).Value = txtCod.Text;               

                //EXECUTA O COMANDO
                mySqlCommand.CommandType = CommandType.Text;
                //RECEBE O CONTEUDO QUE VEM DO BANCO
                MySqlDataReader dr;
                dr = mySqlCommand.ExecuteReader();
                //INSERE AS INFORMAÇOES RECEBIDAS DO BANCO PARA OS COMPONENTES DO FORM
                dr.Read();

                txtNome.Text = dr.GetString(0);
                combEstado.Text = dr.GetString(1);
                combCidade.Text = dr.GetString(2);
                txtEndereco.Text = dr.GetString(3);

                //FECHA A CONEXÃO
                mySqlConnection.Close();
            }
            catch(Exception erro) 
            {
                MessageBox.Show("Erro ao buscar o arquivo" +erro);
            }
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            try {
                //passsa a string de conexão
                MySqlConnection mySqlConnection = new MySqlConnection("server= localhost;" +
                    "port=3306;" +
                    "User Id= root;" +
                    "database=bd_crud;" +
                    "password='';");
                //abre o banco de dados
                mySqlConnection.Open();

                //COMANDO DO MYSQL COM SEUS DEVIDOS PARANMETROS
                MySqlCommand mySqlCommand = new MySqlCommand("update tb_dados set nm_nome = ?, sg_estado = ?, nm_cidade = ?, ds_endereco = ? where  cd_dados = ?", mySqlConnection);
                mySqlCommand.Parameters.Clear();
                mySqlCommand.Parameters.Add("@nm_nome",MySqlDbType.VarChar, 60).Value =txtNome.Text;
                mySqlCommand.Parameters.Add("@sg_estado",MySqlDbType.VarChar, 2).Value =combEstado.SelectedItem.ToString();
                mySqlCommand.Parameters.Add("@nm_cidade",MySqlDbType.VarChar, 20).Value =combEstado.SelectedItem.ToString();
                mySqlCommand.Parameters.Add("@ds_endereco",MySqlDbType.VarChar, 100).Value =txtEndereco.Text;
                mySqlCommand.Parameters.Add("@cd_dados",MySqlDbType.Int32).Value =txtCod.Text;
                
                //EXECULTANDO COMANDO
                mySqlCommand.CommandType = CommandType.Text;
                mySqlCommand.ExecuteNonQuery();

                //FECHA A CONEXÃO
                mySqlConnection.Close();
                MessageBox.Show("Atualização realizada com sucesso");
            }
                catch (Exception erro) {
                MessageBox.Show("Falha ao atualizar "+erro);
            }

        }
    }
}
